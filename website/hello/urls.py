from django.urls import path

from website.hello.views import greeting_view

urlpatterns = [
    path("", view=greeting_view, name="home"),
]
