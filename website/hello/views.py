from django.views.generic.base import TemplateView

from website.hello.models import Greeting


class GreetingView(TemplateView):
    """Greet the user"""

    template_name = "hello/greeting.html"

    def get_greeting(self):
        """Pick a greeting"""
        greeting = Greeting.objects.order_by("pk").first()
        return greeting

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        greeting = self.get_greeting()
        context["greeting"] = greeting
        return context


greeting_view = GreetingView.as_view()
