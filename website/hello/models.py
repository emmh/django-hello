from django.db import models
from django.utils.translation import gettext_lazy as _


class CommonInfo(models.Model):
    class Meta:
        abstract = True

    ctime = models.DateTimeField(auto_now_add=True)
    mtime = models.DateTimeField(auto_now=True)


class Greeting(CommonInfo):
    class Meta:
        verbose_name = _("greeting")
        verbose_name_plural = _("greetings")
        ordering = ["text"]

    text = models.CharField(max_length=120)
