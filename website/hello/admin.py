from django.contrib import admin

from website.hello.models import Greeting


@admin.register(Greeting)
class GreetingAdmin(admin.ModelAdmin):
    list_display = ("text",)
    list_display_links = ("text",)
