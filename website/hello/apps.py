from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class HelloConfig(AppConfig):
    name = "website.hello"
    verbose_name = _("Hello")

    def ready(self):
        try:
            import website.hello.signals  # noqa F401
        except ImportError:
            pass
